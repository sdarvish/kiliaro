package ir.sdarvish.testtask

import android.app.Application
import coil.Coil
import coil.ImageLoader
import coil.disk.DiskCache
import coil.request.CachePolicy
import dagger.hilt.android.HiltAndroidApp
import timber.log.Timber

@HiltAndroidApp
class App : Application() {

    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
        configImageLoader()
    }

    private fun configImageLoader() {
        Coil.setImageLoader(ImageLoader.Builder(this)
            .diskCache {
                DiskCache.Builder().directory(this.cacheDir.resolve("image_cache")).build()
            }
            .diskCachePolicy(CachePolicy.ENABLED)
            .build())
    }
}
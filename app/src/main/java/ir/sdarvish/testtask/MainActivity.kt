package ir.sdarvish.testtask

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import dagger.hilt.android.AndroidEntryPoint
import ir.sdarvish.ui.main.AppMainNavHost
import ir.sdarvish.ui.theme.TestTaskTheme

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            TestTaskTheme {
                AppMainNavHost()
            }
        }
    }
}


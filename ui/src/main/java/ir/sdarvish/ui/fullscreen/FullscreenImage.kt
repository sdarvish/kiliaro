package ir.sdarvish.ui.fullscreen

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.BlurredEdgeTreatment
import androidx.compose.ui.draw.blur
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.AsyncImage
import coil.compose.AsyncImagePainter
import coil.compose.SubcomposeAsyncImage
import coil.compose.SubcomposeAsyncImageContent
import ir.sdarvish.domain.model.ImageMedia
import ir.sdarvish.ui.OverlayText
import ir.sdarvish.ui.theme.PlaceHolderColor
import ir.sdarvish.ui.toThumbnailImageRequest


@Composable
fun FullscreenImageMedia(imageMedia: ImageMedia?) {
    Box(modifier = Modifier.fillMaxSize()) {
        imageMedia?.let { imageMedia ->
            SubcomposeAsyncImage(
                modifier = Modifier.fillMaxSize(),
                model = imageMedia.toThumbnailImageRequest(
                    LocalContext.current,
                    imageMedia.downloadUrl,
                    "down"
                ),
                contentDescription = imageMedia.filename
            ) {
                when (painter.state) {
                    AsyncImagePainter.State.Empty -> {}
                    is AsyncImagePainter.State.Error -> {}

                    is AsyncImagePainter.State.Loading ->
                        Box(modifier = Modifier.fillMaxSize()) {
                            AsyncImage(
                                modifier = Modifier
                                    .blur(
                                        radiusX = 5.dp,
                                        radiusY = 5.dp,
                                        edgeTreatment = BlurredEdgeTreatment.Unbounded
                                    )
                                    .fillMaxSize(),
                                model = imageMedia.toThumbnailImageRequest(
                                    LocalContext.current,
                                    imageMedia.thumbnailUrl,
                                    "thumb"
                                ),
                                contentDescription = ""
                            )
                            CircularProgressIndicator(
                                modifier = Modifier.align(Alignment.Center),
                                color = PlaceHolderColor
                            )
                        }

                    is AsyncImagePainter.State.Success -> {
                        SubcomposeAsyncImageContent()
                    }
                }
            }
            imageMedia.createdAt.takeIf { it.isNotBlank() }?.let {
                OverlayText(
                    modifier = Modifier
                        .align(Alignment.BottomEnd)
                        .padding(16.dp),
                    text = imageMedia.createdAt,
                    fontSize = 14.sp
                )
            }

        }
    }
}
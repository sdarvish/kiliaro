package ir.sdarvish.ui.fullscreen

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import ir.sdarvish.domain.model.ImageMedia
import ir.sdarvish.domain.usecase.GetImageMediaUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class FullScreenViewModel @Inject constructor(private val getImageMediaUseCase: GetImageMediaUseCase) :
    ViewModel() {

    private val _imageMedia = MutableStateFlow<ImageMedia?>(null)

    fun getImageMedia(mediaId: String): StateFlow<ImageMedia?> {
        viewModelScope.launch(Dispatchers.IO) {
            _imageMedia.emit(getImageMediaUseCase.invoke(mediaId))
        }
        return _imageMedia
    }
}
package ir.sdarvish.ui

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import ir.sdarvish.ui.theme.OverlayTextBackground

@Composable
fun OverlayText(
    modifier: Modifier = Modifier,
    text: String,
    textAlign: TextAlign = TextAlign.Center,
    color: Color = Color.Black,
    fontSize: TextUnit = 10.sp,
) {
    Box(
        modifier = modifier
            .padding(8.dp)
            .background(OverlayTextBackground, RoundedCornerShape(12.dp))
    ) {
        Text(
            text = text,
            modifier = Modifier.padding(horizontal = 6.dp, vertical = 4.dp),
            textAlign = textAlign,
            color = color,
            fontSize = fontSize,
        )
    }
}

@Preview
@Composable
fun OverlayTextPreview() {
    OverlayText(text = "2023/08/03 10:20")
}
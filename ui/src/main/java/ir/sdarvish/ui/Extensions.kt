package ir.sdarvish.ui

import android.content.Context
import coil.request.CachePolicy
import coil.request.ImageRequest
import ir.sdarvish.domain.model.ImageMedia

fun ImageMedia.toThumbnailImageRequest(context: Context, url: String, keyPrefix: String) =
    ImageRequest.Builder(context)
        .diskCachePolicy(CachePolicy.ENABLED)
        .diskCacheKey("$keyPrefix-${id}")
        .data(url)
        .build()
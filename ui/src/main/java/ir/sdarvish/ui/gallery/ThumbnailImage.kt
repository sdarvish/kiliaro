package ir.sdarvish.ui.gallery

import android.content.res.Configuration
import androidx.annotation.VisibleForTesting
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.IntSize
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.AsyncImage
import ir.sdarvish.core.appendQueryParam
import ir.sdarvish.domain.model.ImageMedia
import ir.sdarvish.ui.OverlayText
import ir.sdarvish.ui.toThumbnailImageRequest


@Composable
fun ThumbnailsImageMedia(
    modifier: Modifier = Modifier,
    radius: Dp = 4.dp,
    imageMedia: ImageMedia,
    onCLick: (image: ImageMedia) -> Unit
) {
    var size by remember { mutableStateOf(IntSize.Zero) }
    Box(
        modifier = modifier
            .size(85.dp)
            .clip(RoundedCornerShape(radius))
            .clickable { onCLick(imageMedia) }
            .onGloballyPositioned { size = it.size }
            .border(
                width = 0.5.dp,
                color = MaterialTheme.colorScheme.outlineVariant,
                shape = RoundedCornerShape(radius)
            )

    ) {
        val resizedUrl = imageMedia.thumbnailUrl.appendQueryParam(Pair("w", size.width.toString()))
        AsyncImage(
            model = imageMedia.toThumbnailImageRequest(LocalContext.current, resizedUrl, "thumb"),
            contentDescription = imageMedia.filename,
            contentScale = ContentScale.Crop,
            modifier = Modifier
                .clip(RoundedCornerShape(4.dp))
                .align(Alignment.Center)
                .testTag(ThumbnailImageTestTags.IMAGE)
        )
        OverlayText(
            text = imageMedia.size,
            modifier = Modifier.align(Alignment.BottomEnd),
            textAlign = TextAlign.Center,
            color = Color.Black,
            fontSize = 10.sp,
        )
    }
}

@Preview(showBackground = true, backgroundColor = 0xFFFFFF)
@Preview(uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
fun ThumbnailsImageMediaPreview() {
    Surface(Modifier.padding(10.dp)) {
        ThumbnailsImageMedia(
            imageMedia = ImageMedia(
                "1",
                "name",
                "1.1MB",
                "created at",
                "",
                "",
                100,
                190
            )
        ) {
        }
    }

}

@VisibleForTesting
internal object ThumbnailImageTestTags {
    internal const val IMAGE = "@TEST_TAG_THUMBNAIL_IMAGE_TEXT"
}
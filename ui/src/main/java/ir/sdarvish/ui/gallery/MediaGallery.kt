package ir.sdarvish.ui.gallery

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Refresh
import androidx.compose.material3.FilledIconButton
import androidx.compose.material3.Icon
import androidx.compose.material3.LinearProgressIndicator
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import ir.sdarvish.domain.model.ImageMedia
import ir.sdarvish.ui.theme.TestTaskTheme


@Composable
fun MediaGallery(
    modifier: Modifier = Modifier,
    images: List<ImageMedia>,
    isLoadingInProgress: Boolean = false,
    onUpdateRequest: (() -> Unit)? = null,
    onItemClick: (ImageMedia) -> Unit
) {
    Box(modifier = Modifier.fillMaxSize()) {
        LazyVerticalGrid(
            modifier = modifier.padding(16.dp),
            columns = GridCells.Fixed(3),
            verticalArrangement = Arrangement.spacedBy(8.dp),
            horizontalArrangement = Arrangement.spacedBy(8.dp),
        ) {
            items(items = images) {
                ThumbnailsImageMedia(imageMedia = it, onCLick = onItemClick)
            }
        }
        Box(
            Modifier
                .padding(16.dp)
                .align(Alignment.BottomEnd)
        ) {
            FilledIconButton(modifier = Modifier
                .size(40.dp),
                onClick = { onUpdateRequest?.invoke() }) {
                Icon(
                    Icons.Rounded.Refresh,
                    contentDescription = ""
                )

            }
        }

        if (isLoadingInProgress) {
            LinearProgressIndicator(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(4.dp)
            )
        }
    }
}

@Preview(showSystemUi = true)
@Composable
fun MediaGalleryPreview() {
    val images = List(30) {
        ImageMedia(
            id = it.toString(),
            filename = "FILE_NAME_$it.JPG",
            size = "2.9MB",
            createdAt = "24-3-2015",
            thumbnailUrl = "thumbUrl",
            downloadUrl = "downUrl",
            resX = 100,
            resY = 100
        )
    }

    TestTaskTheme {
        Surface {
            MediaGallery(modifier = Modifier, images = images) {}
        }
    }
}


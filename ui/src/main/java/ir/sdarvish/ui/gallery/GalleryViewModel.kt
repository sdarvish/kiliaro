package ir.sdarvish.ui.gallery

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import ir.sdarvish.core.UiEvent
import ir.sdarvish.core.toUiEvent
import ir.sdarvish.domain.model.ImageMedia
import ir.sdarvish.domain.usecase.GetGalleryUseCase
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.emitAll
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class GalleryViewModel @Inject constructor(
    private val getGalleryUseCase: GetGalleryUseCase
) : ViewModel() {

    private val _gallery = MutableStateFlow<List<ImageMedia>>(listOf())
    val gallery: StateFlow<List<ImageMedia>>
        get() = _gallery

    private val _isInProgress = MutableStateFlow(false)
    val isInProgression: StateFlow<Boolean>
        get() = _isInProgress

    private val _uiEvent = MutableSharedFlow<UiEvent>()
    val uiEvent: SharedFlow<UiEvent>
        get() = _uiEvent


    init {
        updateGallery(isNeededToUpdate = false)
    }

    fun updateGallery(isNeededToUpdate: Boolean = false) = viewModelScope.launch {
        _gallery.emitAll(getGalleryUseCase.invoke(isNeededToUpdate).onStart {
            _isInProgress.emit(true)
        }.onCompletion {
            _isInProgress.emit(false)
        }.catch {
            _uiEvent.emit(it.toUiEvent())
        })
    }
}


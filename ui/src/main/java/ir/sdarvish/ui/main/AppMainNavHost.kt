package ir.sdarvish.ui.main

import android.widget.Toast
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.platform.LocalContext
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import ir.sdarvish.core.UiEvent
import ir.sdarvish.ui.fullscreen.FullScreenViewModel
import ir.sdarvish.ui.fullscreen.FullscreenImageMedia
import ir.sdarvish.ui.gallery.GalleryViewModel
import ir.sdarvish.ui.gallery.MediaGallery
import kotlinx.coroutines.flow.filterIsInstance
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

@Composable
fun AppMainNavHost(navController: NavHostController = rememberNavController()) {
    NavHost(navController = navController, startDestination = "gallery") {
        composable("gallery") {
            val viewModel: GalleryViewModel = hiltViewModel()
            val context = LocalContext.current //
            LaunchedEffect("UiEvent") {
                viewModel.uiEvent
                    .filterIsInstance<UiEvent.ShowMessage>().onEach {
                        Toast.makeText(context, it.message, Toast.LENGTH_LONG).show()
                    }.launchIn(this)

            }
            MediaGallery(
                images = viewModel.gallery.collectAsState().value,
                isLoadingInProgress = viewModel.isInProgression.collectAsState().value,
                onUpdateRequest = { viewModel.updateGallery(true) }
            ) {
                navController.navigate("full_screen?media_id=${it.id}")
            }
        }
        composable("full_screen?media_id={id}") { backStackEntry ->
            val viewModel: FullScreenViewModel = hiltViewModel()
            val media = viewModel.getImageMedia(
                backStackEntry.arguments?.getString("id").orEmpty()
            ).collectAsState()
            FullscreenImageMedia(imageMedia = media.value)
        }
    }
}
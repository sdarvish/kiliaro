package ir.sdarvish

import ir.sdarvish.domain.model.ImageMedia


fun prepareImageMedia(
    id: String = "1",
    filename: String = "FILE_NAME",
    size: String = "10MB",
    createdAt: String = "24-3-2015",
    thumbnailUrl: String = "thumbUrl",
    downloadUrl: String = "downUrl",
    resX: Int = 100,
    resY: Int = 100
) = ImageMedia(
    id = id,
    filename = filename,
    size = size,
    createdAt = createdAt,
    thumbnailUrl = thumbnailUrl,
    downloadUrl = downloadUrl,
    resX = resX,
    resY = resY
)
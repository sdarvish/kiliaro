package ir.sdarvish.ui

import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.onNodeWithText
import androidx.compose.ui.test.performClick
import androidx.test.ext.junit.runners.AndroidJUnit4
import io.kotest.matchers.shouldBe
import ir.sdarvish.domain.model.ImageMedia
import ir.sdarvish.prepareImageMedia
import ir.sdarvish.ui.gallery.ThumbnailImageTestTags
import ir.sdarvish.ui.gallery.ThumbnailsImageMedia
import ir.sdarvish.ui.theme.TestTaskTheme
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class ThumbnailImageKtTest {
    @get:Rule
    val composeRule = createComposeRule()

    private val itemSize = "10MB"

    @Test
    fun givenThumbnailImage_WhenItAppears_ThenItShouldDisplayImageAndSize() {
        composeRule.setContent {
            TestTaskTheme {
                ThumbnailsImageMedia(imageMedia = prepareImageMedia(size = itemSize)) {}
            }
        }

        composeRule.onNodeWithTag(ThumbnailImageTestTags.IMAGE, useUnmergedTree = true)
            .assertIsDisplayed()
        composeRule.onNodeWithText(itemSize, useUnmergedTree = true)
            .assertIsDisplayed()
    }

    @Test
    fun giveOnClickAction_WhenImageOrSizeWasTaped_ThenOnClickShouldBeInvoked() {
        val image = prepareImageMedia(size = itemSize)
        var clickedCount = 0
        val onclick: ((ImageMedia) -> Unit) = { clickedCount++ }
        composeRule.setContent {
            TestTaskTheme {
                ThumbnailsImageMedia(imageMedia = image, onCLick = onclick)
            }
        }

        composeRule.onNodeWithTag(ThumbnailImageTestTags.IMAGE, useUnmergedTree = true)
            .performClick()
        composeRule.onNodeWithText(itemSize, useUnmergedTree = true)
            .performClick()
        clickedCount shouldBe 2
    }
}
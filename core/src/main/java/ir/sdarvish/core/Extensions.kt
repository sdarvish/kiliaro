package ir.sdarvish.core

import android.net.Uri
import java.text.DecimalFormat


fun Long.toMB(): String {
    val format = DecimalFormat("##.##MB")
    return format.format(this / (1024 * 1024f))
}


fun String.appendQueryParam(vararg params: Pair<String, String>) = Uri.parse(this).buildUpon()
    .let {
        params.forEach { param ->
            it.appendQueryParameter(param.first, param.second)
        }
        it.build().toString()
    }
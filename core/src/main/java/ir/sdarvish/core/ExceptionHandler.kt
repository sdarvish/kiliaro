package ir.sdarvish.core

fun Throwable.toUiEvent() = when (this) {
    is NetworkException -> this.toNetworkUiEvent()
    else -> UiEvent.ShowMessage(this.message ?: "Something wrong happen!")
}

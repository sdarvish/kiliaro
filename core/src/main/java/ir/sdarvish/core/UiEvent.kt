package ir.sdarvish.core


sealed interface UiEvent {
    data class ShowMessage(val message: String) : UiEvent
}
package ir.sdarvish.core

// todo: It's very very simple way to show errors, it have to  refactor
class NetworkException(val statusCode: Int) : Exception() {
    fun toNetworkUiEvent(): UiEvent {
        val message = when (statusCode) {
            401 -> "You have to authorized to access this request"
            403 -> "You don't have permission"
            404 -> "Not Found!"
            in (500..599) -> "Severe Error"
            else -> "Network Exception!:-("
        }
        return UiEvent.ShowMessage(message)
    }
}
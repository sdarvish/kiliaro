package ir.sdarvish.data.models


import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import java.util.Date

@Entity(tableName = "medias")
@Serializable
data class Media(
    @PrimaryKey
    @SerialName("id")
    val id: String,
    @SerialName("user_id")
    val userId: String,
    @SerialName("media_type")
    val mediaType: String,
    @SerialName("filename")
    val filename: String,
    @SerialName("size")
    val size: Long,
    @SerialName("created_at")
    @Serializable(DateSerializer::class)
    val createdAt: Date?,
    @Serializable(DateSerializer::class)
    @SerialName("taken_at")
    val takenAt: Date?,
    @SerialName("guessed_taken_at")
    @Serializable(DateSerializer::class)
    val guessedTakenAt: Date?,
    @SerialName("md5sum")
    val md5sum: String,
    @SerialName("content_type")
    val contentType: String,
    @SerialName("video")
    val video: String?,
    @SerialName("thumbnail_url")
    val thumbnailUrl: String,
    @SerialName("download_url")
    val downloadUrl: String,
    @SerialName("resx")
    val resx: Int,
    @SerialName("resy")
    val resy: Int
)

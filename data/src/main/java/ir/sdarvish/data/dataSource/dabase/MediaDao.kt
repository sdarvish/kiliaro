package ir.sdarvish.data.dataSource.dabase

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ir.sdarvish.data.models.Media

@Dao
abstract class MediaDao {

    @Query("SELECT * FROM medias;")
    abstract fun getGallery(): List<Media>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insertAll(list: List<Media>)

    @Query("DELETE FROM medias WHERE 1;")
    abstract suspend fun clearAll()

    suspend fun updateLocalCache(list: List<Media>) {
        clearAll()
        insertAll(list)
    }

    @Query("SELECT * FROM medias WHERE id=:mediaId LIMIT 1")
    abstract suspend fun getMediaById(mediaId: String): Media?

}
package ir.sdarvish.data.dataSource

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import io.ktor.client.HttpClient
import io.ktor.client.engine.android.Android
import io.ktor.client.plugins.contentnegotiation.ContentNegotiation
import io.ktor.client.plugins.defaultRequest
import io.ktor.client.plugins.logging.ANDROID
import io.ktor.client.plugins.logging.LogLevel
import io.ktor.client.plugins.logging.Logger
import io.ktor.client.plugins.logging.Logging
import io.ktor.serialization.kotlinx.json.json
import ir.sdarvish.data.dataSource.dabase.KiliaroDatabase
import kotlinx.serialization.json.Json
import javax.inject.Inject
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class DatasourceDIModule {
    @Singleton
    @Provides
    fun provideNetworkClient(userAuthentication: UserAuthentication) = HttpClient(Android) {
        install(Logging) {
            logger = Logger.ANDROID
            level = LogLevel.ALL
        }
        install(ContentNegotiation) {
            json(Json {
                prettyPrint = true
                isLenient = true
            })
        }
        defaultRequest {
            url("https://api1.kiliaro.com/shared/${userAuthentication.getSharedKey()}/")
        }
    }

    @Singleton
    @Provides
    fun provideDatabase(@ApplicationContext context: Context): KiliaroDatabase {
        return Room.databaseBuilder(
            context,
            KiliaroDatabase::class.java,
            "kiliaro_database.db"
        ).build()
    }

    @Singleton
    @Provides
    fun provideMediaDao(database: KiliaroDatabase) = database.mediaDao
}

@Singleton
class UserAuthentication @Inject constructor() {
    // TODO: Save this key in secure place like 'Google secure plugin', local.properties or etc
    fun getSharedKey() = "djlCbGusTJamg_ca4axEVw"
}
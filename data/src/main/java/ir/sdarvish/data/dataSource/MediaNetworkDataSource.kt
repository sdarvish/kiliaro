package ir.sdarvish.data.dataSource

import io.ktor.client.HttpClient
import io.ktor.client.request.get
import io.ktor.client.statement.HttpResponse
import javax.inject.Inject

class MediaNetworkDataSource @Inject constructor(
    private val apiClient: HttpClient
) {
    suspend fun getGallery(): HttpResponse = apiClient.get("mediaasdfasdf")

}
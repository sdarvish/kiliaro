package ir.sdarvish.data.dataSource.dabase

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import ir.sdarvish.data.models.Media

@Database(entities = [Media::class], version = 1)
@TypeConverters(Converters::class)
abstract class KiliaroDatabase : RoomDatabase() {
    abstract val mediaDao: MediaDao
}
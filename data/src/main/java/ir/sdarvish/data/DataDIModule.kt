package ir.sdarvish.data

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import ir.sdarvish.data.repository.MediaRepository
import ir.sdarvish.data.repository.MediaRepositoryImpl

@Module
@InstallIn(SingletonComponent::class)
interface DataDIModule {
    // TODO: Implement real network datasource
    // TODO: Create local cash
    @Binds
    fun bindMediaRepository(repo: MediaRepositoryImpl): MediaRepository
}
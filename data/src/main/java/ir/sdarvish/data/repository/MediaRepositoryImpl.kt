package ir.sdarvish.data.repository

import io.ktor.client.call.body
import ir.sdarvish.core.NetworkException
import ir.sdarvish.data.dataSource.MediaNetworkDataSource
import ir.sdarvish.data.dataSource.dabase.MediaDao
import ir.sdarvish.data.models.Media
import javax.inject.Inject

class MediaRepositoryImpl @Inject constructor(
    private val remoteDataSource: MediaNetworkDataSource,
    private val localDataSource: MediaDao,
) : MediaRepository {
    override fun getCachedGallery() = localDataSource.getGallery()
    override suspend fun getUpdatedList(): List<Media> {
        val newGallery = getRemoteGallery()
        localDataSource.updateLocalCache(newGallery)
        return newGallery

    }

    override suspend fun getCachedMedia(mediaId: String): Media? =
        localDataSource.getMediaById(mediaId)


    override suspend fun getRemoteGallery(): List<Media> {
        val response = remoteDataSource.getGallery()
        if (response.status.value in 200..299)
            localDataSource.updateLocalCache(response.body())
        else throw NetworkException(response.status.value)

        return response.body()
    }
}

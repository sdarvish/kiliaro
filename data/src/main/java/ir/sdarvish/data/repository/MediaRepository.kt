package ir.sdarvish.data.repository

import ir.sdarvish.data.models.Media

interface MediaRepository {
    fun getCachedGallery(): List<Media>
    suspend fun getRemoteGallery(): List<Media>
    suspend fun getUpdatedList(): List<Media>
    suspend fun getCachedMedia(mediaId: String): Media?
}
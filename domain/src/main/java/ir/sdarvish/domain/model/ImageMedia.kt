package ir.sdarvish.domain.model

import ir.sdarvish.core.toMB
import ir.sdarvish.data.models.Media
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

data class ImageMedia(
    val id: String,
    val filename: String,
    val size: String,
    val createdAt: String,
    val thumbnailUrl: String,
    val downloadUrl: String,
    val resX: Int,
    val resY: Int
)

fun Media.mapToGalleryMedia() = ImageMedia(
    id = id,
    filename = filename,
    size = size.toMB(),
    createdAt = createdAt.toPersistableTime(),
    thumbnailUrl = thumbnailUrl,
    downloadUrl = downloadUrl,
    resX = resx,
    resY = resy
)

fun List<Media>.mapToGalleryMedia(): List<ImageMedia> = map { it.mapToGalleryMedia() }

fun Date?.toPersistableTime(): String {
    return this?.let {
        val dataFormatter = SimpleDateFormat("yyyy/MM/dd HH:mm", Locale.getDefault())
        dataFormatter.format(it)
    } ?: ""
}

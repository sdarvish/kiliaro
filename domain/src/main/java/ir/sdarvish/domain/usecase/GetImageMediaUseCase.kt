package ir.sdarvish.domain.usecase

import ir.sdarvish.data.repository.MediaRepository
import ir.sdarvish.domain.model.ImageMedia
import ir.sdarvish.domain.model.mapToGalleryMedia
import javax.inject.Inject

class GetImageMediaUseCase @Inject constructor(
    private val repository: MediaRepository
) {
    suspend operator fun invoke(mediaId: String): ImageMedia? {
        return repository.getCachedMedia(mediaId)?.mapToGalleryMedia()
    }
}
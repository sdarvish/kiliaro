package ir.sdarvish.domain.usecase

import ir.sdarvish.core.IODispatcher
import ir.sdarvish.data.repository.MediaRepository
import ir.sdarvish.domain.model.ImageMedia
import ir.sdarvish.domain.model.mapToGalleryMedia
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

class GetGalleryUseCase @Inject constructor(
    private val repository: MediaRepository,
    private val updateGalleryUseCase: GetUpdatedGalleryUseCase,
    @IODispatcher private val dispatcher: CoroutineDispatcher
) {
    suspend operator fun invoke(isNeededToUpdate: Boolean = false): Flow<List<ImageMedia>> = flow {
        val cachedItems = repository.getCachedGallery().mapToGalleryMedia()
        emit(cachedItems)
        if (cachedItems.isEmpty() || isNeededToUpdate)
            emit(updateGalleryUseCase.invoke())
    }.flowOn(dispatcher)
}




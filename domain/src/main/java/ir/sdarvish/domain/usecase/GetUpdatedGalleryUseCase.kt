package ir.sdarvish.domain.usecase

import ir.sdarvish.core.DefaultDispatcher
import ir.sdarvish.data.repository.MediaRepository
import ir.sdarvish.domain.model.mapToGalleryMedia
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import javax.inject.Inject

class GetUpdatedGalleryUseCase @Inject constructor(
    private val mediaRepository: MediaRepository,
    @DefaultDispatcher private val dispatcher: CoroutineDispatcher
) {
    suspend operator fun invoke() = withContext(dispatcher) {
        return@withContext mediaRepository.getUpdatedList().mapToGalleryMedia()
    }
}
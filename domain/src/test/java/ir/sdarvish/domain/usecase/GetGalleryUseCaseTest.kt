package ir.sdarvish.domain.usecase

import app.cash.turbine.test
import io.kotest.matchers.shouldBe
import io.mockk.coEvery
import io.mockk.every
import io.mockk.mockk
import ir.sdarvish.core.toMB
import ir.sdarvish.data.models.Media
import ir.sdarvish.data.repository.MediaRepository
import ir.sdarvish.domain.model.ImageMedia
import ir.sdarvish.domain.model.toPersistableTime
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import org.junit.Test
import java.util.Calendar
import java.util.Date

class GetGalleryUseCaseTest {

    private val repository: MediaRepository = mockk(relaxed = true)

    @OptIn(ExperimentalCoroutinesApi::class)
    private val dispatcher = UnconfinedTestDispatcher()
    private val updateGalleryUseCase = GetUpdatedGalleryUseCase(repository, dispatcher)
    private val getGalleryUseCase = GetGalleryUseCase(
        repository,
        updateGalleryUseCase,
        dispatcher
    )


    @Test
    fun `When useCase is invoked, then it should mapped repository result and return as flow`() =
        runTest(dispatcher) {
            val itemSize = 2
            every { repository.getCachedGallery() } returns prepareMedia(itemSize)
            getGalleryUseCase.invoke().test {
                expectMostRecentItem() shouldBe prepareImageMedia(itemSize)
            }
        }

    @Test
    fun `When useCase is invoked and update is required, then it should emit cashed data and then get updated date`() =
        runTest(dispatcher) {
            val localCachedSize = 2
            val remoteItemSize = 4
            every { repository.getCachedGallery() } returns prepareMedia(localCachedSize)
            coEvery { repository.getUpdatedList() } returns prepareMedia(remoteItemSize)
            getGalleryUseCase.invoke(isNeededToUpdate = true).test {
                awaitItem() shouldBe prepareImageMedia(localCachedSize)
                awaitItem() shouldBe prepareImageMedia(remoteItemSize)
                awaitComplete()
            }

        }

    @Test
    fun `When useCase is invoked and there is no cached, then it should emit empty list and then get updated date`() =
        runTest(dispatcher) {
            val remoteItemSize = 4
            every { repository.getCachedGallery() } returns listOf()
            coEvery { repository.getUpdatedList() } returns prepareMedia(remoteItemSize)
            getGalleryUseCase.invoke(isNeededToUpdate = true).test {
                awaitItem() shouldBe listOf()
                awaitItem() shouldBe prepareImageMedia(remoteItemSize)
                awaitComplete()
            }

        }


    private fun prepareMedia(size: Int = 2) = List(size) {
        Media(
            id = it.toString(),
            userId = it.toString(),
            mediaType = "image",
            filename = FILE_NAME(it),
            size = FILES_SIZE,
            createdAt = CREATED_DATE,
            takenAt = Date(),
            guessedTakenAt = null,
            md5sum = "md5sum",
            contentType = "image/*",
            video = null,
            thumbnailUrl = THUMBNAIL_URL,
            downloadUrl = DOWNLOAD_URL,
            resx = RES_XY, resy = RES_XY
        )
    }

    private fun prepareImageMedia(size: Int = 2) = List(size) {
        ImageMedia(
            id = it.toString(),
            filename = FILE_NAME(it),
            size = FILES_SIZE.toMB(),
            createdAt = CREATED_DATE.toPersistableTime(),
            thumbnailUrl = THUMBNAIL_URL,
            downloadUrl = THUMBNAIL_URL,
            resX = RES_XY, resY = RES_XY
        )
    }

    companion object {
        private val FILE_NAME: (id: Int) -> String = { "$it.jpg" }
        private val CREATED_DATE = Calendar.getInstance().apply {
            set(2023, 7, 31)
        }.time

        private const
        val THUMBNAIL_URL = "http://thumbnailUrl"
        private const val DOWNLOAD_URL = "http://thumbnailUrl"
        private const val RES_XY = 100
        private const val FILES_SIZE = 1000L
    }

}